import os
import sys
import base64
import json
import hmac
import hashlib
from urllib import request
from urllib.parse import urlencode
from datetime import datetime, timedelta

def base64url_encode(input: bytes):
    return base64.urlsafe_b64encode(input).decode('utf-8').replace('=','')

def jwt(issuer, secret):

    segments = []

    due_date = datetime.now() + timedelta(seconds=3)
    expiry = int(due_date.timestamp())

    header = {"typ": "JWT","alg": "HS256"}
    payload = {"iss" : issuer, "username": "trigger", "exp": expiry}

    json_header = json.dumps(header, separators=(",",":")).encode()
    json_payload = json.dumps(payload, separators=(",",":")).encode()

    segments.append(base64url_encode(json_header))
    segments.append(base64url_encode(json_payload))

    signing_input = ".".join(segments).encode()
    key = secret.encode()
    signature = hmac.new(key, signing_input, hashlib.sha256).digest()

    segments.append(base64url_encode(signature))

    encoded_string = ".".join(segments)

    return encoded_string

def main() -> int:
    issuer = "gitlab"

    url = os.environ['WEBHOOK_MASTER_URL']
    secret = os.environ['JWT_SECRET']
    name = os.environ['WEBHOOK_NAME']

    jwt_str = jwt(issuer, secret)

    data = urlencode({"name": name}).encode()
    req =  request.Request(url, data=data)
    req.add_header("Authorization", "Bearer " + jwt_str)
    resp = request.urlopen(req)

    if resp.code != 200:
        print(resp.code)
        return 1

    return 0

if __name__ == '__main__':
    sys.exit(main())

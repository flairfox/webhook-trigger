FROM python
COPY ./webhook-trigger.py /tmp/webhook-trigger.py
WORKDIR /tmp
ENTRYPOINT ["python", "webhook-trigger.py"]
